<?php require ('cria_sessao.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>WebStore</title>
		<link rel="stylesheet" href="./css/style.css">
		<link rel="stylesheet" href="./css/style_produtos_imagens_temp.css">
		<link rel="stylesheet" type="text/css" href="./css/style_pagamento_img.css" />
		<link rel="shortcut icon" href="./images/shoppingcart.png" />
		<?php require ("./includes/js.inc"); ?>
	</head>
	<body>
		
		<?php include ("includes/menu_superior.inc"); ?>
		
		<div id="main_container">
			<div id="presentation">
				<?php
					include ("./includes/slider.inc");
					include ("./includes/login.inc");
				?>
			</div>

			<div id="main_content">
				<div class="left_content">
					<?php include ("includes/accordion_menu.inc"); ?>
				</div>	<!-- CLOSE: class="left_content" -->

				<div class="center_content">
					<div class="center_title_bar">Fale Conosco (Preencha os campos abaixo para entrar em contato)
							<!--<div class="carrinho">Meu Carrinho
							<div id="interior_carrinho">
								<div id="fundo_interior_carrrinho"> </div>	
							</div>-->	
					</div>
					<form name="contato" action="#" method="post" onsubmit='return validar_contato(this);'>
						<div class="center_prod_box_big">
							<div class="contact_form">
								<div class="form_row">
									<label class="contact"><strong>Nome:</strong></label>
									<input type="text" class="contact_input" />
								</div>
								<div class="form_row">
									<label class="contact"><strong>Email:</strong></label>
									<input type="text" name="email" id="email" class="contact_input" />
								</div>
								<div class="form_row">
									<label class="contact"><strong>Telefone:</strong></label>
									<input type="text" name="telefone" id="telefone" class="contact_input" />
								</div>
								<div class="form_row">
									<label class="contact"> <strong>Mensagem:</strong> </label>
									<input type="text" id="contact_textarea"/>
								</div>
								<div class="form_row">
									<input type="submit" id="enviar" value="Enviar"/>
								</div>
							</div>
						</div>
					</form>
				</div>	<!-- CLOSE: class="center_content" -->

			</div>	<!-- CLOSE: id="main_content" -->	
			<div class="footer"> </div> <!-- NÃO DELETAR PORQUE CARREGA CSS -->		
		</div>	<!-- CLOSE: id="main_container" -->
	</div>
	<?php include ("includes/footer.inc"); ?>
	</body>
</html>
