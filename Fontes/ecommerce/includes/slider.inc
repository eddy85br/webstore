<div id="slider">
	<div id="container">
		<img id="prev" class="prev" src="images/slider/previous.png" />
		<img id="next" class="next" src="./images/slider/next.png" />
		<div class="slider">
			<ul>
				<?php
					$numFotos = 4;
					$inicial = 1;
					for ($indice = $inicial; $indice < ($numFotos + $inicial); $indice++) {
						print '<li><img class="slider" src="./images/slider/'.$indice.'.jpg" alt="image '.$indice.'" /></li>';
					}
				?>
			</ul>
		</div>
	</div>
</div>