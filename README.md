# ESSE É UM ARQUIVO DE README DO PROJETO
### O mesmo serve apenas para o repositório Git no [BitBucket](https://bitbucket.org/eddy85br/webstore)!
## Ler o arquivo "Instalacao.html" no diretório de "Documentacao" para instalar o sistema.

## Dicas para instalar PHP e Apache2 no Linux Mint 15:

1. Rodar: `sudo apt-get install apache2`
2. Aceitar (S ou Y) e deixar rolar até o final
3. Colocar uma cópia da pasta "ecommerce" na pasta do Apache:
    1. Rodar: `sudo cp -R /home/_eduardo_/seu_diretorio/web1/ecommerce /var/www/.`
4. Abrir o "Gerenciador de Pacotes":
    1. Clicar em "Menu" na barra ou apertar a tecla "Windows"
    2. Abrir o segundo icone na lista da esquerda, que parece uma caixa de papelão, abaixo do icone do Firefox.
    3. No campo de busca, procurar por php5 e instalar
    4. [opcional] Pra quem quiser o Chromium e ainda não instalou, procurar por "chromium-browser", mas começando a digitar já vai encontrar, é 1 ícone azulzinho parecido com o Chrome.
5. Abrir o navegador e digitar "localhost/ecommerce"

#### Pronto para programar em PHP...!

## Para instalação do banco de dados, opção "MySQL":  
### Lembrando que o script criará usuário e senha, banco e tabelas, copiar para sua máquina e editar se quiser fazer diferente...

#### Se já existir usuário, senha e o banco, rodar apenas o script\_MySQL.sql na mão (uma das últimas linhas do shell script).

1. Instalar o "mysql-server" pelo "Software Manager" ou com `sudo install apt-get mysql-server` e definir a senha de root.
2. Testar se o servidor do MySQL funciona usando: `mysql -u root -p` e digitar a senha de root (caso não funcione, verificar porquê).
3. Dentro do prompt do mysql:
    1. Verificar DBs: `show databases;`
    2. Conectar no Banco padrão: `use mysql;`
    3. Listar tabelas do Banco: `show tables;`
    4. Listar usuarios: `select * from user;`
    5. Sair (desconectar) do servidor: `\q` ou `exit;`
4. Usar o shell script "criarTudo\_MySQL.sh" no diretório DB: `./criarTudo_MySQL.sh usuario senha script_MySQL.sql`
5. Acessar e verificar se deu tudo certo, conectando com: `mysql -u usuario -d ecommerce -p` (digitar senha do usuario criado)

### Qualquer dúvida, me avisem, para a gente padronizar o banco de dados etc...

Esse é um arquivo Readme no formato Markdown (.md) [mais sobre Markdown...](https://en.wikipedia.org/wiki/Markdown)  
Fim.
